import {
  LOAD_COINS,
  LOAD_COINS_SUCCESS,
  LOAD_COINS_ERROR,
  MAKE_FAVORITE,
  FILTER_FAVORITES
} from './constants';

/**
 * Load all he coins
 *
 * @return {object}
 */
export function loadCoins() {
  return {
    type: LOAD_COINS,
  };
}

/**
 *
 * @param  {array} repos The repository data
 * @param  {string} username The current username
 *
 * @return {object}
 */
export function coinsLoaded(coins) {
  return {
    type: LOAD_COINS_SUCCESS,
    coins
  };
}

/**
 * Dispatched when loading the repositories fails
 *
 * @param  {object} error The error
 *
 * @return {object}       An action object with a type of LOAD_REPOS_ERROR passing the error
 */
export function coinsLoadingError(error) {
  return {
    type: LOAD_COINS_ERROR,
    error
  };
}

/**
 * Changes the input field of the form
 *
 * @param  {name} name The new text of the input field
 *
 * @return {object}    An action object with a type of CHANGE_USERNAME
 */
export function makeFavorite(id) {
  return {
    type: MAKE_FAVORITE,
    id
  };
}

/**
 * Changes the input field of the form
 *
 * @param  {name} name The new text of the input field
 *
 * @return {object}    An action object with a type of CHANGE_USERNAME
 */
export function filterFavorites() {
  return {
    type: FILTER_FAVORITES
  };
}

