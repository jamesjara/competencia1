export const LOAD_COINS = 'boilerplate/App/LOAD_COINS ';
export const LOAD_COINS_SUCCESS = 'boilerplate/App/LOAD_COINS_SUCCESS';
export const LOAD_COINS_ERROR = 'boilerplate/App/LOAD_COINS_ERROR';
export const MAKE_FAVORITE = 'boilerplate/App/MAKE_FAVORITE';
export const FILTER_FAVORITES = 'boilerplate/App/FILTER_FAVORITES';
export const DEFAULT_LOCALE = 'en';
