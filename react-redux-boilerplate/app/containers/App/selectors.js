import { createSelector } from 'reselect';

const selectGlobal = (state) => state.get('global');

const selectRoute = (state) => state.get('route');

const makeSelectLoading = () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('loading')
);

const makeSelectError = () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('error')
);

const makeSelectCoins = () => createSelector(
  selectGlobal,
  (globalState) => globalState.getIn(['coins'])
);

const makeSelectLocation = () => createSelector(
  selectRoute,
  (routeState) => routeState.get('location').toJS()
);

const getFilter = (state) => state.isFiltered

const getFavorites = createSelector(
    [ makeSelectCoins, getFilter ],
    ( coins, filter) => {
      return coins
      // TODO - filter by isFav === true
      //.filter(item => item.isFav === false )
  }
)

export {
  selectGlobal,
  makeSelectLoading,
  makeSelectError,
  makeSelectCoins,
  makeSelectLocation,
  getFavorites
};
