import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import {
  makeSelectCoins,
  makeSelectLoading,
  makeSelectError,
  getFavorites
} from 'containers/App/selectors';
import { loadCoins, makeFavorite, filterFavorites } from '../App/actions';
import reducer from './reducer';
import saga from './saga';
import HomePage from './HomePage';

const mapDispatchToProps = (dispatch) => ({
  onMakeFavorite: (id) => dispatch(makeFavorite(id)),
  onLoadCoin: (evt) => dispatch(makeFavorite(id)),
  onLoadCoins: (evt) => dispatch(loadCoins())
});

const mapStateToProps = createStructuredSelector({
  coins: makeSelectCoins(),
  loading: makeSelectLoading(),
  error: makeSelectError()
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'home', reducer });
const withSaga = injectSaga({ key: 'home', saga });

export default compose(withReducer, withSaga, withConnect)(HomePage);
export { mapDispatchToProps };
