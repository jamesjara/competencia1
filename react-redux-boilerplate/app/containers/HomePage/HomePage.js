/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import ReactTable from 'react-table';
import { LineChart, Line, CartesianGrid } from 'recharts';
import Toggle from 'react-toggle';
import 'react-table/react-table.css';
import './style.scss';

export default class HomePage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  /**
   * when initial state username is not null, submit the form to load repos
   */
  componentDidMount() {
    this.props.onLoadCoins();
  }

  onUpdateChart (data) {
    // TODO: Fake data to test sync with chart, historical data for last 24H is required here.
    const marketData = data.original.quotes['USD'];
    const initialPrice =  (marketData.price * 100) / (100 - Math.abs(marketData.percent_change_24h));
    const dataChunkPrice = 100;//(marketData.price - initialPrice) / 24;

    const chartData = new Array(24);
    chartData[0] = { uv: initialPrice };
    for(let i = 1; i < 24; i++) {
      chartData[i] = {
        uv: chartData[i - 1].uv + dataChunkPrice
      };
    }
    this.setState({
      chartData
    })
  }

  state = {
    isChecked: false
  }

  render() {
    const { loading, error, coins } = this.props;
    const coinsListProps = {
      loading,
      error,
      coins
    };

    const columns = [{
      Header: 'Fav?',
      accessor: 'fav',
      width: 75,
      Cell: props => <span>{props.isFavorite ? '♥': '♡'} </span> // Custom cell components!
    }, {
      Header: 'Coin Name',
      accessor: 'name'
    }, {
      Header: 'Ticker',
      accessor: 'symbol',
      width: 100,
    }, {
      Header: 'Price (USD)',
      accessor: 'quotes.USD.price',
      Cell: row => `$ ${row.value}`
    }, {
      Header: 'Market Cap (USD)',
      accessor: 'quotes.USD.market_cap',
      Cell: row => `$ ${row.value}`
    }, {
      Header: 'Volume (USD)',
      accessor: 'quotes.USD.volume_24h',
      Cell: row => `$ ${row.value}`
    }]
    return (
      <article>
        <Helmet>
        </Helmet>
        <div className="home-page">
          <LineChart width={750} height={300} data={this.state.chartData} margin={{ top: 5, right: 20, bottom: 5, left: 0 }}>
          <Line type="monotone" dataKey="uv" stroke="#8884d8" />
          <CartesianGrid stroke="#ccc" strokeDasharray="5 5" />
        </LineChart>
        <div className="toggle">
          <label>
            <span>Show only favs</span>
            <Toggle
              defaultChecked={this.state.isChecked}
              icons={false}
              onChange={this.handleChecked} />
          </label>
        </div>
        <ReactTable
          data={coins}
          columns={columns}
          getTdProps={(state, rowInfo, column, instance) => {
            return {
              onClick: (e, handleOriginal) => {
                if (handleOriginal) {
                  handleOriginal();
                }
                if(column && column.Header === 'Fav?'){
                  this.props.onMakeFavorite(rowInfo.original.id)
                } else {
                  this.onUpdateChart(rowInfo)
                }
              }
            };
          }}
            />
        </div>
      </article>
    );
  }
  handleChecked = () => (
    this.setState(({ isChecked }) => ({ isChecked: !isChecked }))
  )
}

HomePage.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.bool,
  ]),
  coins: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.bool,
  ]),
  onLoadCoins: PropTypes.func,
  onSubmitForm: PropTypes.func,
  username: PropTypes.string,
  onChangeUsername: PropTypes.func,
};
