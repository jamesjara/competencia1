/**
 * Gets the repositories of the user from Github
 */

import { call, put, select, takeLatest } from 'redux-saga/effects';
import { LOAD_COINS } from 'containers/App/constants';
import { coinsLoaded, coinsLoadingError } from 'containers/App/actions';

import request from 'utils/request';
import { makeSelectUsername } from 'containers/HomePage/selectors';

/**
 * Github repos request/response handler
 */
export function* getCoins(coin) {
  const requestURL = `https://api.coinmarketcap.com/v2/ticker/?structure=array`;

  try {
    // Call our request helper (see 'utils/request')
    const coins = yield call(request, requestURL);
    yield put(coinsLoaded(coins.data, coin));
  } catch (err) {
    yield put(coinsLoadingError(err));
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* coinsRootSaga() {
  // Watches for LOAD_REPOS actions and calls getRepos when one comes in.
  // By using `takeLatest` only the result of the latest API call is applied.
  // It returns task descriptor (just like fork) so we can continue execution
  // It will be cancelled automatically on component unmount
  yield takeLatest(LOAD_COINS, getCoins);
}
